package com.naggadim.helper;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Button deni;
    Intent intent;
    SimpleDateFormat sdf;
    String date;
    final String LOG_TAG = "MainActivity";
    final String DAY_LOG_TAG = "DayChoose";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Секретные антиДенисные технологии", Snackbar.LENGTH_LONG)
                        .setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                deni.setVisibility(View.VISIBLE);
                            }
                        }).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /* Auto generated end */
        deni = (Button) findViewById(R.id.butDenis);
        intent  = new Intent(this, ScheduleActivity.class);
    }

    @Override
    public void onResume () {
        super.onResume();
        intent = null;
        intent = new Intent(this, ScheduleActivity.class);
        sdf = new SimpleDateFormat("EEE");
        date = sdf.format(new Date(System.currentTimeMillis()));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.Check_Update) {
            Toast.makeText(this,"Пока не работает!",Toast.LENGTH_SHORT).show();
        } else if (id == R.id.DevMan) {
            Toast.makeText(this,"Разработчик Дмитрий Смертин (12П) "
                            + " ( NaggaDIM ) ",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_share) {
            Toast.makeText(this,"Пока не работает!",Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_send) {
            Toast.makeText(this,"Пока не работает!",Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /** Обработчик нажатий кнопок */
    public void onClickStart(View v) {
        switch (v.getId()) {
            case R.id.btnToday:
                intent.putExtra("Day", date);
                startActivity(intent);
                Log.d(DAY_LOG_TAG, date);
                break; /** Сегодня */
            case R.id.btnTomorrow:
                if ("пн".equals(date)) {
                    intent.putExtra("Day", "вт");
                    Log.d(DAY_LOG_TAG, date);
                }
                if ("вт".equals(date)) {
                    intent.putExtra("Day", "ср");
                    Log.d(DAY_LOG_TAG, date);
                }
                if ("ср".equals(date)) {
                    intent.putExtra("Day", "чт");
                    Log.d(DAY_LOG_TAG, date);
                }
                if ("чт".equals(date)) {
                    intent.putExtra("Day", "пт");
                    Log.d(DAY_LOG_TAG, date);
                }
                if ("пт".equals(date)) {
                    intent.putExtra("Day", "сб");
                    Log.d(DAY_LOG_TAG, date);
                }
                if ("сб".equals(date)) {
                    intent.putExtra("Day", "вс");
                    Log.d(DAY_LOG_TAG, date);
                }
                if ("вс".equals(date)) {
                    intent.putExtra("Day", "пн");
                    Log.d(DAY_LOG_TAG, date);
                }
                startActivity(intent);
                break; /** Завтра */
            case R.id.btnMon:
                intent.putExtra("Day", "пн");
                startActivity(intent);
                Log.d(DAY_LOG_TAG, "пн");
                break; /** Понедельник */
            case R.id.btnTues:
                intent.putExtra("Day", "вт");
                startActivity(intent);
                Log.d(DAY_LOG_TAG, "вт");
                break; /** Вторник */
            case R.id.btnWed:
                intent.putExtra("Day", "ср");
                startActivity(intent);
                Log.d(DAY_LOG_TAG, "ср");
                break; /** Среда */
            case R.id.btnThurs:
                intent.putExtra("Day", "чт");
                startActivity(intent);
                Log.d(DAY_LOG_TAG, "чт");
                break; /** Четверг */
            case R.id.btnFri:
                intent.putExtra("Day", "пт");
                startActivity(intent);
                Log.d(DAY_LOG_TAG, "пт");
                break; /** Пятница */
            case R.id.btnSat:
                intent.putExtra("Day", "сб");
                startActivity(intent);
                Log.d(DAY_LOG_TAG, "сб");
                break; /** Суббота */
            case R.id.butDenis:
                Toast.makeText(this, "Денис Пидор!", Toast.LENGTH_LONG).show();
                deni.setVisibility(View.INVISIBLE);
                break; /** Денис? */
            case  R.id.btndaycall:
                intent.putExtra("Day", "Звонки");
                startActivity(intent);
                break; /** Звонки */
        }
    }

}
