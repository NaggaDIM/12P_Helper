package com.naggadim.helper;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.util.Log;
import android.widget.RemoteViews;

public class MyWidget extends AppWidgetProvider {

    final static String LOG_TAG = "StHWidget";
    static SimpleDateFormat sdf;
    static String date;

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d(LOG_TAG, "onEnabled");
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Log.d(LOG_TAG, "onUpdate " + Arrays.toString(appWidgetIds));
        for (int id : appWidgetIds) {
            updateWidget(context, appWidgetManager, id);
        }

    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        Log.d(LOG_TAG, "onDeleted " + Arrays.toString(appWidgetIds));
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        Log.d(LOG_TAG, "onDisabled");
    }

    static void updateWidget (Context context, AppWidgetManager appWidgetManager, int widgetID) {
        Log.d(LOG_TAG, "updateWidget " + widgetID);
        RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.widget);
        sdf = new SimpleDateFormat("EEE");
        date = sdf.format(new Date(System.currentTimeMillis()));
        if ("пн".equals(date)) {
            Log.d(LOG_TAG, date);
            widgetView.setTextViewText(R.id.txDayWG, "Понедельник");

            widgetView.setTextViewText(R.id.onePWG, "1. - ");

            widgetView.setTextViewText(R.id.twoPWG, "2. Математика ");

            widgetView.setTextViewText(R.id.threePWG, "3. Химия ");

            widgetView.setTextViewText(R.id.fourPWG, "4. Ин. Яз.1 / Ин. Яз. 2 ");

            widgetView.setTextViewText(R.id.fivePWG, " ");
        }
        if ("вт".equals(date)) {
            Log.d(LOG_TAG, date);
            widgetView.setTextViewText(R.id.txDayWG, "Вторник");

            widgetView.setTextViewText(R.id.onePWG, "1. Русс. Яз. / - ");

            widgetView.setTextViewText(R.id.twoPWG, "2. Русс. Яз. ");

            widgetView.setTextViewText(R.id.threePWG, "3. МХК ");

            widgetView.setTextViewText(R.id.fourPWG, "4. История ");

            widgetView.setTextViewText(R.id.fivePWG, " ");
        }
        if ("ср".equals(date)) {
            Log.d(LOG_TAG, date);
            widgetView.setTextViewText(R.id.txDayWG, "Среда");

            widgetView.setTextViewText(R.id.onePWG, "1. - ");

            widgetView.setTextViewText(R.id.twoPWG, "2. Обществознание ");

            widgetView.setTextViewText(R.id.threePWG, "3. Физ-ра ");

            widgetView.setTextViewText(R.id.fourPWG, "4. Математика ");

            widgetView.setTextViewText(R.id.fivePWG, " ");
        }
        if ("чт".equals(date)) {
            Log.d(LOG_TAG, date);
            widgetView.setTextViewText(R.id.txDayWG, "Четверг");

            widgetView.setTextViewText(R.id.onePWG, "1. Математика ");

            widgetView.setTextViewText(R.id.twoPWG, "2. ОБЖ. ");

            widgetView.setTextViewText(R.id.threePWG, "3. Экология ");

            widgetView.setTextViewText(R.id.fourPWG, "4. История / Физ-ра ");

            widgetView.setTextViewText(R.id.fivePWG, " ");
        }
        if ("пт".equals(date)) {
            Log.d(LOG_TAG, date);
            widgetView.setTextViewText(R.id.txDayWG, "Пятница");

            widgetView.setTextViewText(R.id.onePWG, "1. - ");

            widgetView.setTextViewText(R.id.twoPWG, "2. - ");

            widgetView.setTextViewText(R.id.threePWG, "3. - ");

            widgetView.setTextViewText(R.id.fourPWG, "4. Ин. Яз. 1 / Информ. 2 ");

            widgetView.setTextViewText(R.id.fivePWG, "5. Ин. Яз. 2 / Информ. 1 ");
        }
        if ("сб".equals(date)) {
            Log.d(LOG_TAG, date);
            widgetView.setTextViewText(R.id.txDayWG, "Суббота");

            widgetView.setTextViewText(R.id.onePWG, "1. География / - ");

            widgetView.setTextViewText(R.id.twoPWG, "2. Русс. Яз. / - ");

            widgetView.setTextViewText(R.id.threePWG, "3. Физика ");

            widgetView.setTextViewText(R.id.fourPWG, "4. Астрономия / Русс. Яз. ");

            widgetView.setTextViewText(R.id.fivePWG, " ");
        }
        if ("вс".equals(date)) {
            Log.d(LOG_TAG, date);
            widgetView.setTextViewText(R.id.txDayWG, "Воскресенье");

            widgetView.setTextViewText(R.id.onePWG, " ");

            widgetView.setTextViewText(R.id.twoPWG, " ");

            widgetView.setTextViewText(R.id.threePWG, " Выходной ");

            widgetView.setTextViewText(R.id.fourPWG, " ");

            widgetView.setTextViewText(R.id.fivePWG, " ");
        }
        appWidgetManager.updateAppWidget(widgetID,widgetView);
    }

}
