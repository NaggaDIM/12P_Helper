package com.naggadim.helper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ScheduleActivity extends AppCompatActivity {

    String[] dMon = {"  1. -", "  2. Математика", "  3. Химия", "  4. Ин. Яз 1 / Ин. Яз. 2", "  5. -"};
    String[] dTues = {"  1. Русс. Яз. / - ", "  2. Русс. Яз. ", "  3. МХК ", "  4. История ", "  5. -"};
    String[] dWed = {"  1. -", "  2. Обществознание", "  3. Физ-Ра", "  4. Математика", "  5. -"};
    String[] dThurs = {"  1. Математика", "  2. ОБЖ", "  3. Экология", "  4. История / Физ-Ра", "  5. -"};
    String[] dFri = {"  1. -", "  2. -", "  3. -", "  4. Ин. Яз. 1 / Информ. 2", "  5. Информ. 1 / Ин. Яз. 2"};
    String[] dSat = {"  1. География / -", "  2. Русс. Яз. / - ", "  3. Физика", "  4. Астрономия / Русс. Яз.", "  5. -"};
    String[] dSun = {"  1. - ", "2. -", "3. Выходной ", "4. -", "5. -"};
    String[] dCalls = {"  1. 8:00 - 9:30 ", "  2. 9:50 - 11:20 ", "  3. 11:40 - 13:10 ", "  4. 13:20 - 14:50", "  5. 15:00 - 16:30 "};
    String[] dParse;
    String LOG_TAG = "Schedule";
    String dayChoose;
    Intent intent1 = new Intent(this, itemServiceActivity.class);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedulelayout);
        Intent intent = getIntent();
        dayChoose = intent.getStringExtra("Day");
        ListView lvSchedule = (ListView) findViewById(R.id.lvSchedule);


        if ("пн".equals(dayChoose)) {
            dParse = dMon;
        } else if ("вт".equals(dayChoose)) {
            dParse = dTues;
        } else if ("ср".equals(dayChoose)) {
            dParse = dWed;
        } else if ("чт".equals(dayChoose)) {
            dParse = dThurs;
        } else if ("пт".equals(dayChoose)) {
            dParse = dFri;
        } else if ("сб".equals(dayChoose)) {
            dParse = dSat;
        } else if ("Звонки".equals(dayChoose)) {
            dParse = dCalls;
        } else if ("вс".equals(dayChoose)) {
            dParse = dSun;
        }

        /** Адаптер*/
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_item, dParse);
        lvSchedule.setAdapter(adapter);
        lvSchedule.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d(LOG_TAG, "itemClick: position = " + position + ", id = "
                        + id);
                if ("пн".equals(dayChoose)) {
                    switch (position) {
                        case 0:
                            ChooseStarter("None");
                            break;
                        case 1:
                            ChooseStarter("Матем");
                            break;
                        case 2:
                            ChooseStarter("Хим");
                            break;
                        case 3:
                            ChooseStarter("Инглиш");
                            break;
                        case 4:
                            ChooseStarter("None");
                            break;
                    }
                } else if ("вт".equals(dayChoose)) {
                    switch (position) {
                        case 0:
                            ChooseStarter("Рус");
                            break;
                        case 1:
                            ChooseStarter("Рус");
                            break;
                        case 2:
                            ChooseStarter("МХК");
                            break;
                        case 3:
                            ChooseStarter("Ист");
                            break;
                        case 4:
                            ChooseStarter("None");
                            break;
                    }
                } else if ("ср".equals(dayChoose)) {
                    switch (position) {
                        case 0:
                            ChooseStarter("None");
                            break;
                        case 1:
                            ChooseStarter("Общест");
                            break;
                        case 2:
                            ChooseStarter("Физра");
                            break;
                        case 3:
                            ChooseStarter("Матем");
                            break;
                        case 4:
                            ChooseStarter("None");
                            break;
                    }
                } else if ("чт".equals(dayChoose)) {
                    switch (position) {
                        case 0:
                            ChooseStarter("Матем");
                            break;
                        case 1:
                            ChooseStarter("ОБЖ");
                            break;
                        case 2:
                            ChooseStarter("Экол");
                            break;
                        case 3:
                            ChooseStarter("Инглиш");
                            break;
                        case 4:
                            ChooseStarter("ИСТ/ФИЗ");
                            break;
                    }
                } else if ("пт".equals(dayChoose)) {
                    switch (position) {
                        case 0:
                            ChooseStarter("None");
                            break;
                        case 1:
                            ChooseStarter("None");
                            break;
                        case 2:
                            ChooseStarter("None");
                            break;
                        case 3:
                            ChooseStarter("Инглиш/ИНФ");
                            break;
                        case 4:
                            ChooseStarter("Инглиш/ИНФ");
                            break;
                    }
                } else if ("сб".equals(dayChoose)) {
                    switch (position) {
                        case 0:
                            ChooseStarter("Геогр");
                            break;
                        case 1:
                            ChooseStarter("Рус");
                            break;
                        case 2:
                            ChooseStarter("Физ");
                            break;
                        case 3:
                            ChooseStarter("Аст/Рус");
                            break;
                        case 4:
                            ChooseStarter("None");
                            break;
                    }
                } else if ("Звонки".equals(dayChoose)) {

                } else if ("вс".equals(dayChoose)) {

                }
            }
        });
    }

    private void ChooseStarter(String value){
        intent1.putExtra("Choose", value);
        startActivity(intent1);
    }

    @Override
    public void onResume () {
        super.onResume();
        intent1 = null;
        intent1 = new Intent(this, itemServiceActivity.class);
    }
}